﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagement.Domain
{
    public class PaginationResult<T>
    {
        public IEnumerable<T> data { get; set; }
        public uint Total { get; set; }
    }
}
