﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;

namespace UserManagement.Domain
{
    public class UserResult : Result
    {
        public AppUser user { get; set; }
    }
}
