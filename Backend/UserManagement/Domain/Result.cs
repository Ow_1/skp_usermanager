﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UserManagement.Domain
{
    public class Result<t>
    {
        public bool Success { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public t Data { get; set; }
    }

    public class Result
    {
        public bool Success { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}
