﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public interface IUserService
    {
        Task<AppUser> GetuserById(string userID);

        Task<UserResult> AddUser(AppUser user, string password, AppUser appUser);
        Task<bool> DeleteUser(string userID);
        Task<bool> UpdateUser(string userID, User UpdatedUserInfo);

        Task<IEnumerable<Group>> GetAllGroupsByUser(string userID);
        Task<Result> AddUserToGroup(string userID, Guid groupid);
        Task<Result> RemoveUserToGroup(string userID, Guid groupid);
    }
}
