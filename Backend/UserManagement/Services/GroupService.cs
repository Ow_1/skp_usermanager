﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public class GroupService : IGroupService
    {
        private readonly ApplicationDbContext _context;

        public GroupService(ApplicationDbContext context)
        {
            _context = context;
        }

        public Group GetByID(Guid ID)
        {
            return _context.groups
                .Include(x=>x.Created_by)
            .SingleOrDefault(x => x.ID == ID);
        }

        public ICollection<Group> GetAll()
        {
            return _context.groups.Include(x => x.Created_by).ToList();
        }

        public async Task<Result<Group>> CreateGroup(string name, AppUser user)
        {
            if (user == null)
            {
                return new Result<Group> { Errors = new[] { "User do not exits" } };
            }

            if (String.IsNullOrWhiteSpace(name))
            {
                return new Result<Group> { Errors = new[] { "Name is empty or only white Spaces" } };
            }

            var newGroup = new Group
            {
                Name = name,
                Created_by = user,
            };

            await _context.groups.AddAsync(newGroup);

            await _context.SaveChangesAsync();

            return new Result<Group>
            {
                Success = true,
                Data = newGroup
            };
        }

        public async Task<bool> DeleteGroup(Guid ID)
        {
            Group Group = GetByID(ID);
            if (Group == null)
            {
                return false;
            }

            _context.groups.Remove(Group);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<Result> RenameGroup(Guid groupid, string name)
        {
            if(string.IsNullOrWhiteSpace(name))
                return new Result { Errors = new[] { "name is empty or only whitespaces" } };

            Group Group = GetByID(groupid);
            if (Group == null)
            {
                return new Result { Errors = new[] { "Group do not exist" }};
            }

            Group.Name = name;

            _context.groups.Update(Group);
            await _context.SaveChangesAsync();

            return new Result { Success = true };
        }

        
    }
}
