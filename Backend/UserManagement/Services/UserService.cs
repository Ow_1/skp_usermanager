﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly ApplicationDbContext _context;

        public UserService(UserManager<AppUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<AppUser> GetuserById(string userID)
        {
            return await _context.AppUsers.Where(u => u.Id == userID)
                .Include(user=>user.UserGroups)
                    .ThenInclude(x=>x.Group)
            .SingleOrDefaultAsync();
        }

        public async Task<UserResult> AddUser(AppUser user, string password, AppUser appUser)
        {
            var existingUser = await _userManager.FindByEmailAsync(user.Email);

            if(appUser == null)
            {
                return new UserResult
                {
                    Errors = new[] { "AppUser does not exist" }
                };
            }

            if(existingUser != null)
            {
                return new UserResult
                {
                    Errors = new[] { "User already exist" }
                };
            }

            user.Id = Guid.NewGuid().ToString();
            user.Created_by = appUser;
            var createdUser = await _userManager.CreateAsync(user, password);

            if(!createdUser.Succeeded)
            {
                return new UserResult
                {
                    Errors = createdUser.Errors.Select(x => x.Description)
                };
            }

            return new UserResult
            {
                user = user,
                Success = true
            };
        }
       
        public async Task<bool> DeleteUser(string userID)
        {
            var User = await _userManager.FindByIdAsync(userID);
            if(User == null)
            {
                return false;
            }

            var result = await _userManager.DeleteAsync(User);
            if(!result.Succeeded)
            {
                return false;
            }

            return true;
        }

        public async Task<Result> AddUserToGroup(string userID, Guid groupid)
        {
            var User = await _userManager.FindByIdAsync(userID);
            if (User == null)
            {
                return new Result() { Errors = new[] {"User does not exist"} };
            }

            var Group = _context.groups.Include(x => x.UserGroups).SingleOrDefault(x => x.ID == groupid);
            if (User == null)
            {
                return new Result() { Errors = new[] {"Group is not defind"} };
            }

            if(User.UserGroups.Any(x => x.Group == Group))
            {
                return new Result() { Errors = new[] {"User is already in this group"} };
            }

            var ug = new UserGroup() { Group = Group, User = User };

            Group.UserGroups.Add(ug);
            User.UserGroups.Add(ug);

            _context.groups.Update(Group);
            _context.AppUsers.Update(User);

            await _context.SaveChangesAsync();

            return new Result() { Success = true };
        }

        public async Task<IEnumerable<Group>> GetAllGroupsByUser(string userID)
        {
            var user = await GetuserById(userID);
            if(user == null)
            {
                return null;
            }

            return user.UserGroups.Select(x => new Group()
            {
                ID = x.Group.ID,
                Name = x.Group.Name,
                Created_at = x.Group.Created_at,
                Created_by = x.Group.Created_by,
                UserGroups = x.Group.UserGroups
            });
        }

        public async Task<Result> RemoveUserToGroup(string userID, Guid groupid)
        {
            var User = await GetuserById(userID);
            if (User == null)
            {
                return new Result() { Errors = new[] { "User does not exist" } };
            }

            var ug = User.UserGroups.SingleOrDefault(x => x.GroupId == groupid);
            var Group = ug.Group;
            if (Group == null || ug == null)
            {
                return new Result() { Errors = new[] { "Group does not exist or user is not in this group" } };
            }

            Group.UserGroups.Remove(ug);
            User.UserGroups.Remove(ug);

            _context.groups.Update(Group);
            _context.AppUsers.Update(User);

            await _context.SaveChangesAsync();
            return new Result() { Success = true };
        }

        public async Task<bool> UpdateUser(string userID, User UpdatedUserInfo)
        {
            var User = await _userManager.FindByIdAsync(userID);
            if (User == null)
            {
                return false;
            }

            if (UpdatedUserInfo.Email != null) { User.Email = UpdatedUserInfo.Email; User.UserName = UpdatedUserInfo.Email; }
            if (UpdatedUserInfo.Firstname != null) User.Firtname = UpdatedUserInfo.Firstname;
            if (UpdatedUserInfo.Lastname != null) User.Lastname = UpdatedUserInfo.Lastname;

            var result = await _userManager.UpdateAsync(User);
            if(!result.Succeeded)
            {
                return false;
            }

            return true;
        }
    }
}
