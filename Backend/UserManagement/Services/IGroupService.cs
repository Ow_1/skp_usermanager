﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public interface IGroupService
    {
        Group GetByID(Guid ID);
        ICollection<Group> GetAll();

        Task<Result<Group>> CreateGroup(string name, AppUser user);
        Task<Result> RenameGroup(Guid groupid, string name);
        Task<bool> DeleteGroup(Guid ID);
    }
}
