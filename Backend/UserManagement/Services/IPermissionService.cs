﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Domain;

namespace UserManagement.Services
{
    /// <summary>
    /// Permission
    /// </summary>
    public interface IPermissionService
    {
        AppUser CurrUser { get; set; }

        ICollection<Data.Permission> GetUserPermission(AppUser user);
        ICollection<Data.Permission> GetGroupPermission(Group group);

        Task<Result> AddPermissionToUser(AppUser user, string permission, bool force);
        Task<Result> AddPermissionToGroup(Group group, string permission, bool force);

        Task<Result> RemovePermissionToUser(AppUser user, string permission, bool force);
        Task<Result> RemovePermissionToGroup(Group group, string permission, bool force);
        
        Task<Domain.Permission> CheckForPermission(AppUser user, string permission);
        Task<List<Domain.Permission>> CheckForPermission(AppUser user, string[] permission);
    }
}
