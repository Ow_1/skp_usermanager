﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly ApplicationDbContext _context;
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private AppUser CurrUser;

        public PermissionService(ApplicationDbContext context, IUserService userService, IGroupService groupService)
        {
            _context = context;
            _userService = userService;
            _groupService = groupService;
        }

        public ICollection<Data.Permission> GetUserPermission(AppUser user)
        {
            return _context.permissions.Where(p => p.User == user).ToList();
        }

        public ICollection<Data.Permission> GetGroupPermission(Group group)
        {
            return _context.permissions.Where(p => p.Group == group).ToList();
        }

        public async Task<Result> AddPermissionToUser(AppUser user, string permission, bool force)
        {
            if(string.IsNullOrWhiteSpace(permission))
                return new Result { Errors = new[] { "The Permission is null or empty" } };

            var euser = await _userService.GetuserById(user.Id);
            if(user == null)
                return new Result { Errors = new[] { "The user does not exist" } };

            if(_context.permissions.Any(x=> x.User == user && x.Perm == permission))
                return new Result { Errors = new[] { "The user already has that permission" } };

            if (user == CurrUser && force == false)
                return new Result { Errors = new[] { "You can not edit you own permissions" } };

            var val = Util.ValidatePermission.Validate(permission);
            if (!val.Success)
            {
                return val;
            }

            _context.permissions.Add(new Data.Permission { User = euser, Perm = permission, Given_By = CurrUser });
            await _context.SaveChangesAsync();

            return new Result { Success = true };
        }

        public async Task<Result> AddPermissionToGroup(Group group, string permission, bool force)
        {
            if (string.IsNullOrWhiteSpace(permission))
                return new Result { Errors = new[] { "The Permission is null or empty" } };

            var egroup = _groupService.GetByID(group.ID);
            if (egroup == null)
                return new Result { Errors = new[] { "The group does not exist" } };

            if (_context.permissions.Any(x => x.Group == egroup && x.Perm == permission))
                return new Result { Errors = new[] { "The group already has that permission" } };

            //TODO: Can not edit your own permission

            var val = Util.ValidatePermission.Validate(permission);
            if (!val.Success)
            {
                return val;
            }

            _context.permissions.Add(new Data.Permission { Group = egroup, Perm = permission, Given_By = CurrUser });
            await _context.SaveChangesAsync();

            return new Result { Success = true };
        }

        public async Task<Result> RemovePermissionToUser(AppUser user, string permission, bool force)
        {
            if (string.IsNullOrWhiteSpace(permission))
                return new Result { Errors = new[] { "The Permission is null or empty" } };

            var euser = await _userService.GetuserById(user.Id);
            if (user == null)
                return new Result { Errors = new[] { "The user does not exist" } };
            
            if(user == CurrUser && force == false)
                return new Result { Errors = new[] { "You can not edit you own permissions" } };

            var permis = _context.permissions.SingleOrDefault(x => x.User == user && x.Perm == permission);

            if (permis == null)
                return new Result { Errors = new[] { "The user do not have the permission" }};

            _context.permissions.Remove(permis);
            await _context.SaveChangesAsync();

            return new Result { Success = true };
        }

        public async Task<Result> RemovePermissionToGroup(Group group, string permission, bool force)
        {
            if (string.IsNullOrWhiteSpace(permission))
                return new Result { Errors = new[] { "The Permission is null or empty" } };

            var egroup = _groupService.GetByID(group.ID);
            if (egroup == null)
                return new Result { Errors = new[] { "The group does not exist" } };

            //TODO can not edit if the user is in the group

            var permis = _context.permissions.SingleOrDefault(x => x.Group == group && x.Perm == permission);

            if (permis == null)
                return new Result { Errors = new[] { "The group do not have the permission" } };

            _context.permissions.Remove(permis);
            await _context.SaveChangesAsync();

            return new Result { Success = true };
        }

        public async Task<Domain.Permission> CheckForPermission(AppUser user, string permission)
        {
            return (await CheckForPermission(user, new[] { permission }))[0];
        }

        //TODO: Return a messeage
        public async Task<List<Domain.Permission>> CheckForPermission(AppUser user, string[] permission)
        {
            List<Domain.Permission> p = permission.Select(x=> new Domain.Permission { permission = x, acess = false }).ToList();
            if(!user.isActive)
            {
                return p;
            }

            List<Data.Permission> permissions = new List<Data.Permission>();
            permissions.AddRange(GetUserPermission(user));

            var groups = await _userService.GetAllGroupsByUser(user.Id);
            foreach (var group in groups) 
                permissions.AddRange(GetGroupPermission(group));

            for (int i = 0; i < p.Count; i++)
            {
                if (!Util.ValidatePermission.Validate(p[i].permission).Success)
                    return new List<Domain.Permission>();

                for (int y = 0; y < permissions.Count; y++)
                {
                    if(Util.ValidatePermission.CheckPermission(permissions.ElementAt(y).Perm, p[i].permission))
                    {
                        p[i].acess = true;
                        break;
                    }
                }
            }

            return p;
        }

        AppUser IPermissionService.CurrUser { get => CurrUser; set => CurrUser = value; }
    }
}
