﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UserManagement.Domain;

namespace UserManagement.Util
{
    public static class ValidatePermission
    {
        public static Result Validate(string permission)
        {
            if(permission == "*") return new Result { Success = true };
            MatchCollection mathes = Regex.Matches(permission, @"[A-Za-z]+(\.[A-Za-z\*]+)+");
            if (mathes.Count != 1)
                return new Result { Errors = new[] { "Something is wrong with permission" } };
            if (mathes[0].Length != permission.Length)
                return new Result { Errors = new[] { "Can't endt with ." } };

            return new Result { Success = true };
        }

        public static bool CheckPermission(string a, string b)
        {
            if (a == b) return true;
            if (a == "*") return true;

            var aArr = a.ToLower().Split('.');
            var bArr = b.ToLower().Split('.');

            for (int i = 0; i < aArr.Length; i++)
            {
                if (aArr[i] == "*") return true;
                if(bArr[i] != aArr[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
