﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UserManagement.Data
{
    public class Permission
    {
        [Key]
        public Guid ID { get; set; } = new Guid();

        public AppUser User { get; set; }
        public Group Group { get; set; }
        public string Perm { get; set; }

        public AppUser Given_By { get; set; }
        public DateTime Given_at { get; set; } = DateTime.Now;
    }
}
