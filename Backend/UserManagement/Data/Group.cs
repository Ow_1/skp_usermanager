﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Data;

namespace UserManagement.Data
{
    public class Group
    {
        [Key]
        [MaxLength(85)]
        public Guid ID { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public DateTime Created_at { get; set; } = DateTime.Now;
        [Required]
        public AppUser Created_by { get; set; }

        public DateTime last_Updated_at { get; set; } = DateTime.Now;
        public AppUser last_Updated_by { get; set; }
        
        public ICollection<UserGroup> UserGroups { get; set; } = new List<UserGroup>();
    }

    public class UserGroup
    {
        public string UserId { get; set; }
        public AppUser User { get; set; }

        public Guid GroupId { get; set; }
        public Group Group { get; set; }
    }
}
