﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagement.Data
{
    public class AppUser : IdentityUser
    {
        [Required]
        public string Firtname { get; set; }
        [Required]
        public string Lastname { get; set; }

        public bool isActive { get; set; } = true;
        
        [Required]
        public AppUser Created_by { get; set; }
        public DateTime Created_at { get; set; } = DateTime.Now;

        public ICollection<UserGroup> UserGroups { get; set; } = new List<UserGroup>();
    }
}
