﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using UserManagement.Services;

namespace UserManagement.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class Access : Attribute, IAsyncActionFilter
    {
        private readonly string _permission;

        public Access(string permission)
        {
            _permission = permission;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
            var PermissionService = context.HttpContext.RequestServices.GetRequiredService<IPermissionService>();
            
            var identity = context.HttpContext.User.Identity as ClaimsIdentity;
            {
                if(!identity.IsAuthenticated)
                {
                    context.Result = new UnauthorizedResult();
                    return;
                }
                PermissionService.CurrUser = await userService.GetuserById(identity.FindFirst("id").Value);
            }

            if(!(await PermissionService.CheckForPermission(PermissionService.CurrUser, _permission)).acess)
            {
                context.Result = new ForbidResult(JwtBearerDefaults.AuthenticationScheme);
                return;
            }

            await next();
            return;
        }
    }
}
