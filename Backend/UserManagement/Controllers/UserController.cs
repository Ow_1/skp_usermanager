﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Contracts;
using UserManagement.Contracts.User;
using UserManagement.Data;
using UserManagement.Domain;
using UserManagement.Filters;
using UserManagement.Services;

namespace UserManagement.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _UserService;
        private readonly UserManager<AppUser> _userManager;

        public UserController(IUserService UserService, UserManager<AppUser> userManager)
        {
            _UserService = UserService;
            _userManager = userManager;
        }

        /// <summary>
        /// Gets all the user in the system
        /// </summary>
        /// <response code="200">Gets all the user</response>
        [Access("UserManager.User.Get")]
        [HttpGet(ApiRoutes.User.GetAllUsers)]
        public async Task<IActionResult> GetAllUsers(uint liment = 10, uint page = 1, string query = "")
        {
            IQueryable<AppUser> users = _userManager.Users;

            if (!string.IsNullOrWhiteSpace(query))
            {
                query = query.ToLower();
                users = users.Where(u => 
                    u.Firtname.ToLower().Contains(query) || 
                    u.Lastname.ToLower().Contains(query) || 
                    (u.Firtname + ' ' + u.Lastname).ToLower().Contains(query));
            }

            uint total = (uint)users.Count();
            users = users.Skip((int)((page - 1) * liment)).Take((int)liment);

            return Ok(new PaginationResult<getAllUserResponse>() {
                data = users.Select(x => new getAllUserResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    Firstname = x.Firtname,
                    Lastname = x.Lastname,
                    isLocked = x.LockoutEnabled
                }),
                Total = total
            }); 
        }

        /// <summary>
        /// return the authorize user
        /// </summary>
        [Authorize]
        [HttpGet(ApiRoutes.User.UserMe)]
        public async Task<IActionResult> GetMe()
        {
            var user = await GetCurrentUserAsync();
            return Ok(new getMeResponse()
            {
                Id = user.Id,
                Email = user.Email,
                Firstname = user.Firtname,
                Lastname = user.Lastname,
                isLocked = user.LockoutEnabled
            });
        }

        /// <summary>
        /// Gets all the infomation on the user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [Access("UserManager.User.Get")]
        [HttpGet(ApiRoutes.User.GetUser)]
        public async Task<IActionResult> GetUserByID([FromRoute]string userID)
        {
            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            var user = await _UserService.GetuserById(userID);
            if(user == null)
            {
                return NotFound(new Result
                {
                    Errors = new[] { "user was not found" }
                });
            }


            return Ok(new getUserResponse() 
            {
                Id = user.Id,
                Firstname = user.Firtname,
                Lastname = user.Lastname,
                Email = user.Email,
                isLocked = user.LockoutEnabled
            });
        }

        /// <summary>
        /// Create a user
        /// </summary>
        /// <response code="201">successfully create a user</response>
        /// <response code="400">Something is invalid</response>
        [Access("UserManager.User.Managed")]
        [HttpPost(ApiRoutes.User.AddUser)]
        public async Task<IActionResult> AddUser([FromBody] AddUserRequst request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var Result = await _UserService.AddUser(new AppUser
            {
                Firtname = request.Firstname,
                Lastname = request.Lastname,
                UserName = request.Email,
                Email = request.Email
            }, request.Password, await GetCurrentUserAsync());

            if (!Result.Success)
            {
                return BadRequest(new Result
                {
                    Errors = Result.Errors
                });
            }

            //TODO: make a where you can find the user
            //Make this to Created
            return Created("", new AddUserResponse()
            {
                Id = Result.user.Id,
                Email = Result.user.Email,
                Firstname = Result.user.Firtname,
                Lastname = Result.user.Lastname,
                isLocked = Result.user.LockoutEnabled,
                Created_at = Result.user.Created_at,
                Created_by = Result.user.Created_by.Id
                
            });

        }

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <response code="200">the user has successfully deleted</response>
        /// <response code="404">Can not delete the user or user do not exist</response>
        [Access("UserManager.User.Managed")]
        [HttpDelete(ApiRoutes.User.DeleteUser)]
        public async Task<IActionResult> DeleteUser([FromRoute]string userID)
        {
            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            var Result = await _UserService.DeleteUser(userID);
            if (Result)
                return Ok();

            return NotFound();
        }

        /// <summary>
        /// Update a user
        /// </summary>
        /// <response code="200">successfully updated the user</response>
        /// <response code="400">Can't not update the user</response>
        [Access("UserManager.User.Managed")]
        [HttpPut(ApiRoutes.User.UpdateUser)]
        public async Task<IActionResult> UpdateUser([FromRoute]string userID, [FromBody] UpdateUserRequst requst)
        {
            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            bool result = await _UserService.UpdateUser(userID, new Domain.User
            {
                Firstname = requst.Firstname,
                Lastname = requst.Lastname,
                Email = requst.Email
            });

            if (result)
                return Ok();

            return NotFound();
        }

        /// <summary>
        /// Edit password if know the password
        /// </summary>
        [Access("UserManager.User.Password")]
        [HttpPut(ApiRoutes.User.UpdateUserPassword)]
        public async Task<IActionResult> UpdatePassword([FromRoute]string userID, [FromBody] UpdatePasswordRequst requst)
        {
            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var user = await _UserService.GetuserById(userID);
            if(user == null)
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "user does not exist" }
                });
            }

            var result = await _userManager.ChangePasswordAsync(user, requst.CurrentPassword, requst.NewPassword);
            if(!result.Succeeded)
            {
                return BadRequest(new Result
                {
                    Errors = result.Errors.Select(x => x.Description)
                });
            }

            return Ok();
        }

        /// <summary>
        /// Edit password of don't know password
        /// </summary>
        [Access("UserManager.User.Password.Other")]
        [HttpPut(ApiRoutes.User.UnknownUserPassword)]
        public async Task<IActionResult> UpdateOthersPassword([FromRoute]string userID, [FromBody] UpdateOtherPasswordRequst requst)
        {
            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var user = await _UserService.GetuserById(userID);
            if (user == null)
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "user does not exist" }
                });
            }

            var RemoveResult = await _userManager.RemovePasswordAsync(user);
            if (!RemoveResult.Succeeded)
            {
                return BadRequest(new Result
                {
                    Errors = RemoveResult.Errors.Select(x => x.Description)
                });
            }
            var addResult = await _userManager.AddPasswordAsync(user, requst.NewPassword);
            if (!addResult.Succeeded)
            {
                return BadRequest(new Result
                {
                    Errors = addResult.Errors.Select(x => x.Description)
                });
            }

            return Ok();
        }

        /// <summary>
        /// Gets all the groups the specik user is part of
        /// </summary>
        /// <response code="200">Gets all groups by user</response>
        /// <response code="400">Can't find the user</response>
        [Access("UserManager.User.Get")]
        [HttpGet(ApiRoutes.User.GetAllGroupByUser)]
        public async Task<IActionResult> GetUserGroup([FromRoute]string userID)
        {
            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            var groups = await _UserService.GetAllGroupsByUser(userID);

            return Ok(groups.Select(x => new GetUserGroupsResponse()
            {
                ID = x.ID,
                Name = x.Name,
                Created_at = x.Created_at,
                Created_by = x.Created_by.Id
            }));
        }
        
        /// <summary>
        /// Addes a group to a user
        /// </summary>
        [Access("UserManager.User.Managed")]
        [HttpPut(ApiRoutes.User.AddUserToGroup)]
        public async Task<IActionResult> AddUserToGroup([FromRoute]string userID, [FromBody] AddUserToGroupRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            var result = await _UserService.AddUserToGroup(userID, requst.GroupID);
            if (!result.Success)
            {
                return BadRequest(result);
            }

            return Ok();
        }

        /// <summary>
        /// Remove a user from a group
        /// </summary>
        [Access("UserManager.User.Managed")]
        [HttpDelete(ApiRoutes.User.RemoveUserFromGroup)]
        public async Task<IActionResult> RemoveUserToGroup([FromRoute]string userID, [FromBody] RemoveUserToGroupRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            if (string.IsNullOrWhiteSpace(userID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "userID is null or empty" }
                });
            }

            var result = await _UserService.RemoveUserToGroup(userID, requst.GroupID);
            if(!result.Success)
            {
                return BadRequest(result);
            }

            return Ok();
        }

        private async Task<AppUser> GetCurrentUserAsync()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                return await _userManager.FindByIdAsync(identity.FindFirst("id").Value);
            }

            return null;
        }
    }
}