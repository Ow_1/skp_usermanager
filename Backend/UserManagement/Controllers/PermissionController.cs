﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Contracts;
using UserManagement.Contracts.Permission;
using UserManagement.Data;
using UserManagement.Domain;
using UserManagement.Filters;
using UserManagement.Services;

namespace UserManagement.Controllers
{
    public class PermissionController : Controller
    {
        public readonly IPermissionService _permissionService;
        public readonly IUserService _userService;
        public readonly IGroupService _groupService;

        public PermissionController(IPermissionService PermissionService, IUserService userService, IGroupService groupService)
        {
            _userService = userService;
            _permissionService = PermissionService;
            _groupService = groupService;
        }

        /// <summary>
        /// Get all permission by user
        /// </summary>
        [Access("UserManager.Permission.Get")]
        [HttpGet(ApiRoutes.Permission.GetUserPermission)]
        public async Task<IActionResult> GetUserPermission([FromRoute] string userID)
        {
            var res = await IsUserValid(userID);
            if (!res.Success)
            {
                return BadRequest(new Result
                {
                    Errors = res.Errors
                });
            }

            var permissions = _permissionService.GetUserPermission(res.Data);

            return Ok(permissions.Select(x => new GetUserPermissionResponse()
            {
                permission = x.Perm
            }));
        }

        /// <summary>
        /// Add a Permission to a user
        /// </summary>
        [Access("UserManager.Permission.Managed")]
        [HttpPost(ApiRoutes.Permission.AddUserPermission)]
        public async Task<IActionResult> AddUserPermission([FromRoute] string userID, [FromBody] AddUserPermissionRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            
            var res = await IsUserValid(userID);
            if (!res.Success)
            {
                return BadRequest(new Result
                {
                    Errors = res.Errors
                });
            }

            //TODO: regex the permission
            await GetUser(_permissionService);

            var result = await _permissionService.AddPermissionToUser(res.Data, requst.Permission, false);

            if (!result.Success)
            {
                return BadRequest(new Result
                {
                    Errors = result.Errors
                });
            }

            return Ok();
        }

        /// <summary>
        /// Remove a permission from user
        /// </summary>
        [Access("UserManager.Permission.Managed")]
        [HttpPost(ApiRoutes.Permission.RemoveUserPermission)]
        public async Task<IActionResult> RemoveUserPermission([FromRoute] string userID, [FromBody] RemoveUserPermissionRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var res = await IsUserValid(userID);
            if (!res.Success)
            {
                return BadRequest(new Result
                {
                    Errors = res.Errors
                });
            }

            await GetUser(_permissionService);
            var result = await _permissionService.RemovePermissionToUser(res.Data, requst.Permission, false);

            if (!result.Success)
            {
                return BadRequest(new Result
                {
                    Errors = result.Errors
                });
            }

            return Ok();
        }

        /// <summary>
        /// Get all permission by group
        /// </summary>
        [Access("UserManager.Permission.Get")]
        [HttpGet(ApiRoutes.Permission.GetGroupPermission)]
        public async Task<IActionResult> GetGroupPermission([FromRoute] string groupID)
        {
            var res = await IsGroupValid(groupID);
            if (!res.Success)
            {
                return BadRequest(new Result
                {
                    Errors = res.Errors
                });
            }

            var permissions = _permissionService.GetGroupPermission(res.Data);
            return Ok(permissions.Select(x => new GetGroupPermissionResponse()
            {
                permission = x.Perm
            }));
        }

        /// <summary>
        /// Add permission to group
        /// </summary>
        [Access("UserManager.Permission.Managed")]
        [HttpPost(ApiRoutes.Permission.AddGroupPermission)]
        public async Task<IActionResult> AddGroupPermission([FromRoute] string groupID, [FromBody] AddGroupPermissionRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }


            var res = await IsGroupValid(groupID);
            if (!res.Success)
            {
                return BadRequest(new Result
                {
                    Errors = res.Errors
                });
            }

            await GetUser(_permissionService);

            var result = await _permissionService.AddPermissionToGroup(res.Data, requst.Permission, false);

            if (!result.Success)
            {
                return BadRequest(new Result
                {
                    Errors = result.Errors
                });
            }

            return Ok();
        }

        /// <summary>
        /// Remove Permssion from group
        /// </summary>
        [Access("UserManager.Permission.Managed")]
        [HttpPost(ApiRoutes.Permission.RemoveGroupPermission)]
        public async Task<IActionResult> RemoveGroupPermission([FromRoute] string groupID, [FromBody] RemoveGroupPermissionRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var res = await IsGroupValid(groupID);
            if (!res.Success)
            {
                return BadRequest(new Result
                {
                    Errors = res.Errors
                });
            }

            await GetUser(_permissionService);
            var result = await _permissionService.RemovePermissionToGroup(res.Data, requst.Permission, false);

            if (!result.Success)
            {
                return BadRequest(new Result
                {
                    Errors = result.Errors
                });
            }

            return Ok();
        }
        
        /// <summary>
        /// Check a permission
        /// </summary>
        [Access("UserManager.Permission.Get")]
        [HttpPost(ApiRoutes.Permission.CheckUserPermission)]
        public async Task<IActionResult> CheckUserPermission([FromRoute] string userID, [FromBody] CheckUserPermissionRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var res = await IsUserValid(userID);
            if (!res.Success)
                return BadRequest(new Result { Errors = res.Errors });

            var result = await _permissionService.CheckForPermission(res.Data, requst.Permission);
            return Ok(new CheckUserPermissionResponse { Permission = result.ToArray() });
        }

        private async Task<Result<AppUser>> IsUserValid(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return new Result<AppUser> { Errors = new[] { "Something is worng with userid" } };
            var user = await _userService.GetuserById(userId);
            if (user == null)
                return new Result<AppUser> { Errors = new[] { "user with that id does not exist" } };

            return new Result<AppUser> { Success = true, Data = user };
        }

        private async Task<Result<Group>> IsGroupValid(string groupId)
        {
            if (!Guid.TryParse(groupId, out Guid gid))
                return new Result<Group> { Errors = new[] { "Something is worng with groupid" } };
            var group = _groupService.GetByID(gid);
            if (group == null)
                return new Result<Group> { Errors = new[] { "group with that id does not exist" } };

            return new Result<Group> { Success = true, Data = group };
        }

        private async Task GetUser(IPermissionService PermissionService)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            
            var user = await _userService.GetuserById(identity.FindFirst("id").Value);

            if (user == PermissionService.CurrUser)
            {
                return;
            }

            PermissionService.CurrUser = user;
        }
    }
}