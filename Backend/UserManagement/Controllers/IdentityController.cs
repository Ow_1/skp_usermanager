﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Contracts;
using UserManagement.Contracts.Identity;
using UserManagement.Services;

namespace UserManagement.Controllers
{
    public class IdentityController : Controller
    {
        private readonly IIdentityService _identityService;

        public IdentityController(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        /// <summary>
        /// Login to the system
        /// </summary>
        /// <response code="200">Gets the token</response>
        /// <response code="400">The Login detail is invalid or user do not exist</response>
        [HttpPost(ApiRoutes.Identity.Login)]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest request)
        {
            var authResponse = await _identityService.LoginAsync(request.Email, request.Password);

            if(!authResponse.Success)
            {
                return BadRequest(new AuthFailedResponse
                {
                    Errors = authResponse.Errors
                });
            }

            return Ok(new AuthSuccessResponse
            {
                Token = authResponse.Token,
            });
        }

        //[HttpPost(ApiRoutes.Identity.Refresh)]
        //public async Task<IActionResult> Refresh([FromBody] RefreshTokenRequest request)
        //{
        //    var authResponse = await _identityService.RefreshTokenAsync(request.Token, request.RefreshToken);

        //    if (!authResponse.Success)
        //    {
        //        return BadRequest(new AuthFailedResponse
        //        {
        //            Errors = authResponse.Errors
        //        });
        //    }

        //    return Ok(new AuthSuccessResponse
        //    {
        //        Token = authResponse.Token,
        //        RefreshToken = authResponse.RefreshToken
        //    });
        //}
    }
}