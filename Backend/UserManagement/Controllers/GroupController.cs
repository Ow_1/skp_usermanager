﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using UserManagement.Contracts;
using UserManagement.Contracts.Group;
using UserManagement.Data;
using UserManagement.Domain;
using UserManagement.Filters;
using UserManagement.Services;

namespace UserManagement.Controllers
{
    public class GroupController : Controller
    {
        public readonly IGroupService _groupService;
        private readonly UserManager<AppUser> _userManager;

        public GroupController(IGroupService groupService, UserManager<AppUser> userManager)
        {
            _groupService = groupService;
            _userManager = userManager;
        }

        /// <summary>
        /// Gets all the groups in the system
        /// </summary>
        /// <response code="200">Gets all the user</response>
        [Access("UserManager.Group.GetAll")]
        [HttpGet(ApiRoutes.Group.GetAllGroups)]
        public IActionResult GetAll()
        {
            return Ok(_groupService.GetAll().Select(x => new AllGroupRespons()
            {
                ID = x.ID,
                Name = x.Name,
                Created_at = x.Created_at,
                Created_by = x.Created_by?.Id
            }));
        }

        /// <summary>
        /// Get a specific group by id
        /// </summary>
        [Access("UserManager.Group.GetAll")]
        [HttpGet(ApiRoutes.Group.GetGroup)]
        public async Task<IActionResult> GetGroup([FromRoute]string groupID)
        {
            if (string.IsNullOrWhiteSpace(groupID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "groupID is null or empty" }
                });
            }

            var group =  _groupService.GetByID(Guid.Parse(groupID));
            if (group == null)
            {
                return NotFound(new Result
                {
                    Errors = new[] { "group was not found" }
                });
            }

            return Ok(new GetGroupResponse()
            {
                ID = group.ID,
                Name = group.Name,
                Created_at = group.Created_at,
                Created_by = group.Created_by.Id
            });
        }

        /// <summary>
        /// Create a Group
        /// </summary>
        /// <response code="201">successfully create a user</response>
        /// <response code="400">Something is invalid</response>
        [Access("UserManager.Group.Managed")]
        [HttpPost(ApiRoutes.Group.AddGroup)]
        public async Task<IActionResult> AddGroup([FromBody] AddGroupRequst requst)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(new Result
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            AppUser currentuser = await GetCurrentUserAsync();

            var result = await _groupService.CreateGroup(requst.name, currentuser);
            if(!result.Success)
            {
                return BadRequest(new Result
                {
                    Errors = result.Errors
                });
            }

            return Created("", new AddGroupRespons()
            {
                ID = result.Data.ID,
                Name = result.Data.Name,
                Created_at = result.Data.Created_at,
                Created_by = result.Data.Created_by.Id
            });

        }

        /// <summary>
        /// Delete a group
        /// </summary>
        /// <response code="200">the group has successfully deleted</response>
        /// <response code="404">Can not delete the group</response>
        [Access("UserManager.Group.Managed")]
        [HttpDelete(ApiRoutes.Group.DeleteGroup)]
        public async Task<IActionResult> DeleteGroup([FromRoute]string groupID)
        {
            if (string.IsNullOrWhiteSpace(groupID))
            {
                return BadRequest(new Result
                {
                    Errors = new[] { "groupID is null or empty" }
                });
            }

            var Result = await _groupService.DeleteGroup(Guid.Parse(groupID));
            
            if (Result)
                return Ok();

            return NotFound();
        }

        /// <summary>
        /// Rename a group
        /// </summary>
        [Access("UserManager.Group.Managed")]
        [HttpPut(ApiRoutes.Group.RenameGroup)]
        public async Task<IActionResult> RenameGroup([FromRoute]Guid groupID, [FromBody] RenameGroupRequst requst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new Result()
                {
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var result = await _groupService.RenameGroup(groupID, requst.name);

            if(!result.Success)
            {
                return BadRequest(result);
            }

            return Ok();
        }

        private async Task<AppUser> GetCurrentUserAsync()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                return await _userManager.FindByIdAsync(identity.FindFirst("id").Value);
            }

            return null;
        }
    }
}