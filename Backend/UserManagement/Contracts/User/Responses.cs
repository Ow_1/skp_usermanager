﻿using System;

namespace UserManagement.Contracts.User
{
    public class getAllUserResponse
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public bool isLocked { get; set; }
    }

    public class getUserResponse
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public bool isLocked { get; set; }
    }

    public class getMeResponse
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public bool isLocked { get; set; }
    }

    public class AddUserResponse
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public DateTime Created_at { get; set; }
        public string Created_by { get; set; }
        public bool isLocked { get; set; }
    }

    public class GetUserGroupsResponse
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime Created_at { get; set; }
        public string Created_by { get; set; }
    }
}
