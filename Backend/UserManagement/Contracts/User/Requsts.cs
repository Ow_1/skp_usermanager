﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UserManagement.Contracts.User
{
    public class AddUserRequst
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class UpdateUserRequst
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }

    public class AddUserToGroupRequst
    {
        public Guid GroupID { get; set; }
    }

    public class RemoveUserToGroupRequst
    {
        public Guid GroupID { get; set; }
    }

    public class UpdatePasswordRequst
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class UpdateOtherPasswordRequst
    {
        public string NewPassword { get; set; }
    }
}
