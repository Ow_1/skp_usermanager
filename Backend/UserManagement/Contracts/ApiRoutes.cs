﻿namespace UserManagement.Contracts
{
    public static class ApiRoutes
    {
        public const string Base = "api";

        public static class Identity
        {
            public const string Login = Base + "/identity/login";
            public const string Refresh = Base + "/identity/refresh";
        }

        public static class User
        {
            public const string GetAllUsers = Base + "/Users";
            public const string AddUser = Base + "/User/Add";
            public const string UserMe = Base + "/User/Me";
            public const string GetUser = Base + "/User/{userID}";
            public const string DeleteUser = Base + "/User/{userID}/Remove";
            public const string UpdateUser = Base + "/User/{userID}/Update";
            public const string UpdateUserPassword = Base + "/User/{userID}/EditPassword";
            public const string UnknownUserPassword = Base + "/User/{userID}/EditPassword/Unknown";

            public const string AddUserToGroup = Base + "/User/{userID}/Group/Add";
            public const string RemoveUserFromGroup = Base + "/User/{userID}/Groups";
            public const string GetAllGroupByUser = Base + "/User/{userID}/Groups";
        }

        public static class Group
        {
            public const string GetAllGroups = Base + "/Groups";
            public const string AddGroup = Base + "/Group/Add";
            public const string RenameGroup = Base + "/Group/{groupID}/Rename";
            public const string DeleteGroup = Base + "/Group/{groupID}/Remove";
            public const string GetGroup = Base + "/Group/{groupID}";
        }

        public static class Permission
        {
            public const string GetUserPermission = Base + "/User/{userID}/Permission";
            public const string GetGroupPermission = Base + "/Group/{groupID}/Permission";

            public const string AddUserPermission = Base + "/User/{userID}/Permission/Add";
            public const string AddGroupPermission = Base + "/Group/{groupID}/Permission/Add";
            
            public const string RemoveUserPermission = Base + "/User/{userID}/Permission/Remove";
            public const string RemoveGroupPermission = Base + "/Group/{groupID}/Permission/Remove";

            public const string CheckUserPermission = Base + "/User/{userID}/Permission/Check";
        }
    }
}
