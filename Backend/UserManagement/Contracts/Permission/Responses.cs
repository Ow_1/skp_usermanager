﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagement.Contracts.Permission
{
    public class GetUserPermissionResponse
    {
        public string permission;
    }

    public class GetGroupPermissionResponse
    {
        public string permission;
    }

    public class CheckUserPermissionResponse
    {
        public Domain.Permission[] Permission { get; set; }
    }


}
