﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagement.Contracts.Permission
{
    
    public class AddUserPermissionRequst
    {
        public string Permission { get; set; }
    }

    public class RemoveUserPermissionRequst
    {
        public string Permission { get; set; }
    }

    public class AddGroupPermissionRequst
    {
        public string Permission { get; set; }
    }

    public class RemoveGroupPermissionRequst
    {
        public string Permission { get; set; }
    }

    public class CheckUserPermissionRequst
    {
        public string[] Permission { get; set; }
    }
}
