﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagement.Contracts.Group
{
    public class AddGroupRequst
    {
        [Required]
        public string name { get; set; }
    }

    public class RenameGroupRequst
    {
        [Required]
        public string name { get; set; }
    }
}
