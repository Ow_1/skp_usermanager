﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagement.Contracts.Group
{
    public class AllGroupRespons
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime Created_at { get; set; }
        public string Created_by { get; set; }
    }

    public class AddGroupRespons
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime Created_at { get; set; }
        public string Created_by { get; set; }
    }

    public class GetGroupResponse
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime Created_at { get; set; }
        public string Created_by { get; set; }
    }
}
