﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using UserManagement.Contracts;
using UserManagement.Contracts.Group;
using UserManagement.Contracts.Identity;
using UserManagement.Contracts.Permission;
using UserManagement.Contracts.User;
using UserManagement.Data;

namespace UserManagement.IntegrationTests
{
    public class IntergrationTest
    {
        protected readonly HttpClient TestClient;

        protected IntergrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        services.RemoveAll(typeof(ApplicationDbContext));
                        services.AddDbContext<ApplicationDbContext>(options => { options.UseInMemoryDatabase("TestDb"); });
                    });
                });

            TestClient = appFactory.CreateClient();
        }

        protected async Task LoginAsync(string Email, string Password)
        {
            TestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", await GetJwtAsync(Email, Password));
        }

        protected async Task<AddUserResponse> CreateUser()
        {
            return await (await TestClient.PostAsJsonAsync(ApiRoutes.User.AddUser, new AddUserRequst
            {
                Firstname = "Interfration",
                Lastname = "Test",
                Email = "Test@Interfration.test",
                Password = "Passw0rd!"
            })).Content.ReadAsAsync<AddUserResponse>();
        }

        protected async Task GivePermission(string userid, string permission)
        {
            await TestClient.PostAsJsonAsync(ApiRoutes.Permission.AddUserPermission.Replace("{userID}", userid),
                new AddUserPermissionRequst { Permission = permission });
        }

        protected async Task<AddGroupRespons> CreateGroup(string name = "InterGroup")
        {
            return await (await TestClient.PostAsJsonAsync(ApiRoutes.Group.AddGroup, new AddGroupRequst
            {
                name = name
            })).Content.ReadAsAsync<AddGroupRespons>();
        }

        protected async Task LoginAsRootAsync()
        {
            await LoginAsync("root@root.com", "Passw0rd!");
        }

        private async Task<string> GetJwtAsync(string Email, string Password)
        {
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Identity.Login, new UserLoginRequest
            {
                Email = Email,
                Password = Password
            });

            var LoginResponse = await response.Content.ReadAsAsync<AuthSuccessResponse>();
            return LoginResponse.Token;
        }
    }
}
