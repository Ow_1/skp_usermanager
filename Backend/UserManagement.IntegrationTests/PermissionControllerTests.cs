﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Contracts;
using UserManagement.Contracts.Permission;
using UserManagement.Contracts.User;
using Xunit;

namespace UserManagement.IntegrationTests
{
    public class PermissionControllerTests : IntergrationTest
    {
        #region GetUserPermissions
        [Fact]
        public async Task GetUserPermissions_GetAll_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var usr = await CreateUser();
            //TODO: give permission

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Permission.GetUserPermission.Replace("{userID}", usr.Id.ToString()));

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True((await response.Content.ReadAsAsync<List<GetUserPermissionResponse>>()).Count == 0);
        }
        #endregion
        #region AddUserPermissions
        [Fact]
        public async Task AddUserPermissions_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();

            string permission = "test.test";

            // Act
            var res1 = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.AddUserPermission.Replace("{userID}", user.Id.ToString()), 
                new AddUserPermissionRequst { Permission = permission });
            var res2 = await TestClient.GetAsync(ApiRoutes.Permission.GetUserPermission.Replace("{userID}", user.Id.ToString()));
            // Assert
            Assert.Equal(HttpStatusCode.OK, res1.StatusCode);
            Assert.Equal(HttpStatusCode.OK, res2.StatusCode);

            Assert.True((await res2.Content.ReadAsAsync<List<GetUserPermissionResponse>>()).First().permission == permission);
        }
        [Fact]
        public async Task AddUserPermissions_WithMoredot_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();

            string permission = "test.test.test";

            // Act
            var res1 = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.AddUserPermission.Replace("{userID}", user.Id.ToString()),
                new AddUserPermissionRequst { Permission = permission });
            var res2 = await TestClient.GetAsync(ApiRoutes.Permission.GetUserPermission.Replace("{userID}", user.Id.ToString()));
            // Assert
            Assert.Equal(HttpStatusCode.OK, res1.StatusCode);
            Assert.Equal(HttpStatusCode.OK, res2.StatusCode);

            Assert.True((await res2.Content.ReadAsAsync<List<GetUserPermissionResponse>>()).First().permission == permission);
        }
        [Fact]
        public async Task AddUserPermissions_WithSymplos_ReturnsBadRequstResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();

            string permission = "test.te&st";

            // Act
            var res1 = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.AddUserPermission.Replace("{userID}", user.Id.ToString()),
                new AddUserPermissionRequst { Permission = permission });
            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, res1.StatusCode);
        }
        [Fact]
        public async Task AddUserPermissions_Withmoretest_ReturnsBadRequstResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();

            string permission = "test..test";

            // Act
            var res1 = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.AddUserPermission.Replace("{userID}", user.Id.ToString()),
                new AddUserPermissionRequst { Permission = permission });
            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, res1.StatusCode);
        }
        [Fact]
        public async Task AddUserPermissions_WithExtradot_ReturnsBadRequstResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();

            string permission = "test.test.";

            // Act
            var res1 = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.AddUserPermission.Replace("{userID}", user.Id.ToString()),
                new AddUserPermissionRequst { Permission = permission });
            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, res1.StatusCode);
        }
        #endregion
        #region RemoveUserPermissions
        [Fact]
        public async Task RemoveUserPermissions_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();

            string permission = "test.test";
            await GivePermission(user.Id, permission);

            // Act
            var firstRespone = await TestClient.GetAsync(ApiRoutes.Permission.GetUserPermission.Replace("{userID}", user.Id.ToString()));
            var RemoveResponse = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.RemoveUserPermission.Replace("{userID}", user.Id.ToString()),
                new RemoveUserPermissionRequst { Permission = permission });
            var secoundResponse = await TestClient.GetAsync(ApiRoutes.Permission.GetUserPermission.Replace("{userID}", user.Id.ToString()));
            
            // Assert
            Assert.Equal(HttpStatusCode.OK, firstRespone.StatusCode);
            Assert.Equal(HttpStatusCode.OK, RemoveResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, secoundResponse.StatusCode);

            Assert.False((await firstRespone.Content.ReadAsAsync<List<GetUserPermissionResponse>>()).Count == 
                         (await secoundResponse.Content.ReadAsAsync<List<GetUserPermissionResponse>>()).Count);
        }
        #endregion
        #region CheckUserPermissions
        [Fact]
        public async Task CheckUserPermissions_ReturnsOkResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();
            await GivePermission(user.Id, "test.*");

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.CheckUserPermission.Replace("{userID}", user.Id.ToString()),
                new CheckUserPermissionRequst { Permission = new string[] { "test.test", "test.no", "no.test"} });

            var perm = await response.Content.ReadAsAsync<CheckUserPermissionResponse>();

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True(perm.Permission[0].acess);
            Assert.True(perm.Permission[1].acess);
            Assert.False(perm.Permission[2].acess);
        }
        #endregion

        #region AddGroupPermissions
        [Fact]
        public async Task AddGroupPermissions_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var group = await CreateGroup();

            string permission = "test.test";

            // Act
            var res1 = await TestClient.PostAsJsonAsync(ApiRoutes.Permission.AddGroupPermission.Replace("{groupID}", group.ID.ToString()),
                new AddGroupPermissionRequst { Permission = permission});
            var res2 = await TestClient.GetAsync(ApiRoutes.Permission.GetUserPermission.Replace("{groupID}", group.ID.ToString()));
            // Assert
            Assert.Equal(HttpStatusCode.OK, res1.StatusCode);
            Assert.Equal(HttpStatusCode.OK, res2.StatusCode);

            Assert.True((await res2.Content.ReadAsAsync<List<GetUserPermissionResponse>>()).First().permission == permission);
        }
        #endregion
    }
}

//[Fact]
//public async Task GetUser_WithUserId_ReturnsNotFoundResponse()
//{
//    // Arrange
//    // Act
//    // Assert
//    Assert.Equal(HttpStatusCode.OK, response.StatusCode);
//}
