﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using UserManagement.Contracts;
using Xunit;
using System.Net.Http;
using UserManagement.Contracts.User;

namespace UserManagement.IntegrationTests
{
    public class UserControllerTests : IntergrationTest
    {
        #region AddUser
        [Fact]
        public async Task AddUser_WithCurrectFormat_ReturnsCreatedResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.User.AddUser, value: new AddUserRequst 
            { 
                Firstname = "Interfration",
                Lastname = "Test",
                Email = "Test@Interfration.test",
                Password = "Passw0rd!"
            });

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task AddUser_WithIncorrectEmail_ReturnsBadRequstResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.User.AddUser, value: new AddUserRequst
            {
                Firstname = "Interfration",
                Lastname = "Test",
                Email = "TestInterfration.test",
                Password = "Passw0rd!"
            });

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task AddUser_WithMissingPassword_ReturnsBadRequstResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.User.AddUser, value: new AddUserRequst
            {
                Firstname = "Interfration",
                Lastname = "Test",
                Email = "TestInterfration.test",
            });

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task AddUser_WithIncorrectPassword_ReturnsBadRequstResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.User.AddUser, value: new AddUserRequst
            {
                Firstname = "Interfration",
                Lastname = "Test",
                Email = "TestInterfration.test",
                Password = "p"
            });

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        #endregion

        #region DeleteUser
        [Fact]
        public async Task DeleteUser_WithNoneExstingUserId_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await (await TestClient.PostAsJsonAsync(ApiRoutes.User.AddUser, new AddUserRequst
            {
                Firstname = "Interfration",
                Lastname = "Test",
                Email = "Test@Interfration.test",
                Password = "Passw0rd!"
            })).Content.ReadAsAsync<AddUserResponse>();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.User.DeleteUser.Replace("{userID}", user.Id));

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
        
        [Fact]
        public async Task DeleteUser_WithUserId_ReturnsNotFoundResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.User.DeleteUser.Replace("{userID}", "5"));

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
        #endregion
        
        #region GetAll
        [Fact]
        public async Task GetAll_WithOnlyRoot_ReturnsOnlyOneResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.User.GetAllUsers);

            // Assert
            var resdata = await response.Content.ReadAsAsync<Domain.PaginationResult<getAllUserResponse>>();

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True(resdata.Total == 1);
        }
        #endregion

        #region GetUser
        [Fact]
        public async Task GetUser_WithUserId_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await (await TestClient.PostAsJsonAsync(ApiRoutes.User.AddUser, new AddUserRequst
            {
                Firstname = "Interfration",
                Lastname = "Test",
                Email = "Test@Interfration.test",
                Password = "Passw0rd!"
            })).Content.ReadAsAsync<AddUserResponse>();

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.User.GetUser.Replace("{userID}", user.Id.ToString()));

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True(user.Email == (await response.Content.ReadAsAsync<getUserResponse>()).Email);
        }

        [Fact]
        public async Task GetUser_WithUserId_ReturnsNotFoundResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.User.GetUser.Replace("{userID}", "intt"));

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
        #endregion

        #region UpdateUser
        [Fact]
        public async Task UpdateUser_WithUserIdWithOnlyFirstname_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var user = await CreateUser();

            // Act
            var response = await TestClient.PutAsJsonAsync(ApiRoutes.User.UpdateUser.Replace("{userID}", user.Id), new UpdateUserRequst 
            {
                Firstname = "UpdateTest"
            });

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var UserNow = await (await TestClient.GetAsync(ApiRoutes.User.GetUser.Replace("{userID}", user.Id))).Content.ReadAsAsync<getUserResponse>();

            Assert.NotEqual(user.Firstname, UserNow.Firstname);
        }
        #endregion
    }
}

//[Fact]
//public async Task GetUser_WithUserId_ReturnsNotFoundResponse()
//{
//    // Arrange
//    // Act
//    // Assert
//}
