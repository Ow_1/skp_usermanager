﻿using System.Threading.Tasks;
using UserManagement.Contracts;
using Xunit;

namespace UserManagement.IntegrationTests
{
    public class IdentityControllerTests : IntergrationTest
    {
        [Fact]
        public async Task GetAll_NotAuth_Return401()
        {
            // Arrange
            // Act
            var response = await TestClient.GetAsync(ApiRoutes.User.GetAllUsers);

            // Assert
            //response.StatusCode
            Assert.Equal(System.Net.HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async Task GetAll_isAuth_Return200()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.User.GetAllUsers);

            // Assert
            //response.StatusCode
            Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
        }
    }
}
