﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Contracts;
using UserManagement.Contracts.Group;
using Xunit;

namespace UserManagement.IntegrationTests
{
    public class GroupControllerTests : IntergrationTest
    {
        #region GetGroup
        [Fact]
        public async Task GetGroup_withGroupid_returnsOKResponse()
        {
            // arrange
            await LoginAsRootAsync();
            var group = await CreateGroup();

            // act
            var response = await TestClient.GetAsync(ApiRoutes.Group.GetGroup.Replace("{groupID}", group.ID.ToString()));
            
            // assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var resposeGroup = await response.Content.ReadAsAsync<GetGroupResponse>();

            Assert.NotNull(resposeGroup.Created_by);

        }
        #endregion
        #region GetAll
        [Fact]
        public async Task GetAllGroup_WithOneGroup_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            await CreateGroup("test");

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Group.GetAllGroups);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True((await response.Content.ReadAsAsync<List<AllGroupRespons>>()).Count == 1);
        }
        [Fact]
        public async Task GetAllGroup_WithNoGroup_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Group.GetAllGroups);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Empty((await response.Content.ReadAsAsync<List<AllGroupRespons>>()));
        }
        #endregion
        #region AddGroup
        [Fact]
        public async Task AddGroup_WithName_ReturnsOkResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            string name = "test";

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Group.AddGroup, new AddGroupRequst
            {
                name = name
            });

            var g = await response.Content.ReadAsAsync<AddGroupRespons>();

            // Assert
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.True(g.Name == name);
        }

        #endregion
        #region DeleteGroup
        [Fact]
        public async Task DeleteUser_WithGroupId_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var group = await CreateGroup();

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.Group.DeleteGroup.Replace("{groupID}", group.ID.ToString()));

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
        #endregion
        #region RenameGroup
        [Fact]
        public async Task RenameGroup_WithGroupId_ReturnsOKResponse()
        {
            // Arrange
            await LoginAsRootAsync();
            var group = await CreateGroup();

            // Act
            var response = await TestClient.PutAsJsonAsync(ApiRoutes.Group.RenameGroup.Replace("{groupID}", group.ID.ToString()), new RenameGroupRequst
            {
                name = "UpdateTest"
            });

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var GroupNow = await (await TestClient.GetAsync(ApiRoutes.Group.GetGroup.Replace("{groupID}", group.ID.ToString()))).Content.ReadAsAsync<GetGroupResponse>();

            Assert.NotEqual(group.Name, GroupNow.Name);
        }
        #endregion
    }
}

//[Fact]
//public async Task GetUser_WithUserId_ReturnsNotFoundResponse()
//{
//    // Arrange
//    // Act
//    // Assert
//}