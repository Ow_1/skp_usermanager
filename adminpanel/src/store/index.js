import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { stat } from "fs";

Vue.use(Vuex);

const baseurl = "http://localhost:7001";

export default new Vuex.Store({
  state: {
    token: localStorage.getItem("access_token") || null,
    me: {
      id: '',
      firstname: '',
      lastname: ''
    }
  },
  getters: {
    LoggedIn(state) {
      return state.token !== null;
    }
  },
  mutations: {
    retrieveToken(state, token) {
      state.token = token;
    },
    retrieveMe(state, data)
    {
      state.me.id = data.id
      state.me.firstname = data.firstname
      state.me.lastname = data.lastname
    }
  },
  actions: {
    destroyToken() {
      return new Promise((resolve) => {
        localStorage.removeItem('access_token');
        resolve();
      })
    },
    retrieveToken(context, credentials) {
      return new Promise((resolve, reject) => {
        axios.post(`${baseurl}/api/identity/login`, {
            email: credentials.email,
            password: credentials.password
          })
          .then(Response => {
            const token = Response.data.token

            localStorage.setItem('access_token', token)
            context.commit('retrieveToken', token)
            
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            axios.get(`${baseurl}/api/User/Me`)
            .then(res => {
              context.commit('retrieveMe', res.data)
            })

            resolve(Response)
          })
          .catch(error => {
            console.log(error);
            reject(error);
          });
      });
    }
  },
  modules: {}
});
