import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: '/app/dashboard'
  },
  {
    path: "/app",
    name: "app",
    component: () => import("./components/Layout.vue"),
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import("./pages/Dashboard/Dashboard.vue"),
      },
      {
        path: 'users',
        name: 'users',
        component: () => import("./pages/Users/Users.vue"),
      },
      {
        path: 'users/Edit/:id',
        component: () => import("./pages/Users/EditUser.vue")
      },
      {
        path: 'groups',
        name: 'groups',
        component: () => import("./pages/Groups/Groups.vue"),
      },
      {
        path: 'groups/Edit/:id',
        component: () => import("./pages/Groups/EditGroup.vue")
      },
    ]
  },
  {
    path: "/login",
    name: "login",
    component: () => import("./pages/Auth/Login.vue"),
    meta: {
      allowAnonymous: true
    }
  },
  {
    path: "/logout",
    name: "logout",
    component: () => import("./pages/Auth/Logout.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) =>
{
  if(!to.matched.some(record => record.meta.allowAnonymous )) {
    //TODO: Validate about login
    if(!store.getters.LoggedIn) {
      next({name: 'login'})
    } else {
      next()
    }
  }
  next();
})

export default router;
